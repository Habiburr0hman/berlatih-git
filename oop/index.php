<?php

require_once("Animal.php");
require_once("Ape.php");
require_once("Frog.php");

$sheep = new Animal("shaun");

echo "Name: " . $sheep->name . "<br>"; // "shaun"
echo "Legs: " . $sheep->legs . "<br>"; // 4
echo "Cold blooded: " . $sheep->cold_blooded . "<br>"; // "no"

echo "<br>";

$kodok = new Frog("buduk");

echo "Name: " . $kodok->name . "<br>";
echo "Legs: " . $kodok->legs . "<br>";
echo "Cold blooded: " . $kodok->cold_blooded . "<br>";
echo "Jump: ";
echo $kodok->jump(); // "hop hop"

echo "<br><br>";

$sungokong = new Ape("kera sakti");

echo "Name: " . $sungokong->name . "<br>";
echo "Legs: " . $sungokong->legs . "<br>";
echo "Cold blooded: " . $sungokong->cold_blooded . "<br>";
echo "Yell: ";
echo $sungokong->yell(); // "Auooo"

?>
